<?php namespace Ahajji\Tests;

use Ahajji\Messager\Defaults\Inflector;
use PHPUnit\Framework\TestCase;
use Tests\Stubs\MessageBusStubCommand;
use Tests\Stubs\MessageBusStubQuery;
use Tests\Stubs\MessageBusWithoutHandlerStubCommand;

class DefaultInflectTest extends TestCase
{
    /** @var Inflector */
    private $inflector;

    public function setUp()
    {
        $this->inflector = new Inflector;
    }

    /** @test */
    public function should_get_handler_name_from_command()
    {
        $command = new MessageBusStubCommand();

        $handler = $this->inflector->inflect($command);

        $this->assertEquals('Tests\Stubs\MessageBusStubHandler', $handler);
    }

    /** @test */
    public function should_get_handler_name_from_query()
    {
        $command = new MessageBusStubQuery();

        $handler = $this->inflector->inflect($command);

        $this->assertEquals('Tests\Stubs\MessageBusStubHandler', $handler);
    }

    /** @test
     * @expectedException \Ahajji\Messager\Exception\HandlerNotFoundException
     */
    public function should_throw_exception_when_no_handler_for_command()
    {
        $command = new MessageBusWithoutHandlerStubCommand();

        $handler = $this->inflector->inflect($command);
    }
}