<?php

namespace Ahajji\Messager;


interface Sanitizer
{
    /**
     * @param $message
     * @return mixed
     * @throws InvalidMessageException
     */
    public function sanitize($message);
}