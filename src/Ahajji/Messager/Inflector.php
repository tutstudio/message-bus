<?php namespace Ahajji\Messager;

interface Inflector {

    /**
     * Translate a message to its handler counterpart.
     *
     * @param string $message
     * @return string
     * @throws Exception
     */
    public function inflect($message);
}