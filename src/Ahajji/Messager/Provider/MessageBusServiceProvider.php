<?php namespace Ahajji\Messager\Provider;

use Illuminate\Support\ServiceProvider;

class MessageBusServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerInflector();

        $this->registerMessageBus();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['message-bus'];
    }

    /**
     * Register the command translator binding.
     */
    protected function registerInflector()
    {
        $this->app->bind('Ahajji\Messager\Inflector', 'Ahajji\Messager\Defaults\Inflector');
    }

    /**
     * Register the command bus implementation.
     */
    protected function registerMessageBus()
    {
        $this->app->bind('Ahajji\Messager\MessageBus', function($app)
        {
            return $app->make('Ahajji\Messager\Defaults\MessageBus');
        });
    }
}
