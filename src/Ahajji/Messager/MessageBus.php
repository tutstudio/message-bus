<?php namespace Ahajji\Messager;

interface MessageBus {

    /**
     * Execute the command
     *
     * @param $message
     * @param Sanitizer[] $sanitizers
     * @return mixed
     */
    public function handle($message, array $sanitizers = []);
}