<?php namespace Ahajji\Messager\Contracts;

interface Handler {

    /**
     * Handle the command.
     *
     * @param $command
     * @return mixed
     */
    public function handle($command);

} 