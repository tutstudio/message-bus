<?php namespace Ahajji\Messager\Defaults;

use Ahajji\Messager\Exception\HandlerNotFoundException;
use Ahajji\Messager\Inflector as InflactorInterface;

class Inflector implements InflactorInterface {

    /**
     * Translate a message to its handler
     *
     * @param string $message
     * @return string
     * @throws HandlerNotFoundException
     */
    public function inflect($message)
    {
        $class = get_class($message);
        $handler = substr_replace($class, 'Handler', $this->getMessageWordPosition($class));

        if (!class_exists($handler))
        {
            $error = "Message handler [$handler] does not exist.";

            throw new HandlerNotFoundException($error);
        }

        return $handler;
    }

    /**
     * @param string $class
     * @return bool|int
     */
    private function getMessageWordPosition($class)
    {
        if (strrpos($class, 'Command')) {
            return strrpos($class, 'Command');
        }

        return strrpos($class, 'Query');
    }
} 