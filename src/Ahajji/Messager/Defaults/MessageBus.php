<?php namespace Ahajji\Messager\Defaults;

use Ahajji\Messager\Inflector;
use Ahajji\Messager\MessageBus as Message;
use Ahajji\Messager\Sanitizer;
use Illuminate\Contracts\Foundation\Application;
use InvalidArgumentException;

class MessageBus implements Message {

    /**
     * @var Application
     */
    protected $app;

    /**
     * @var Inflector
     */
    protected $inflector;

    /**
     * List of optional decorators for command bus.
     *
     * @var Sanitizer[]
     */
    protected $sanitizers;

    /**
     * MessageBus constructor.
     * @param Application $app
     * @param Inflector $inflector
     */
    function __construct(Application $app, Inflector $inflector)
    {
        $this->app = $app;
        $this->inflector = $inflector;
    }

    /**
     * Execute the command
     *
     * @param $message
     * @param Sanitizer[] $sanitizers
     * @return mixed
     */
    public function handle($message, array $sanitizers = [])
    {
        $this->sanitizers = $sanitizers;

        $this->executeSanitizers($message);

        $handler = $this->inflector->inflect($message);

        return $this->app->make($handler)->handle($message);
    }

    /**
     * Execute all registered decorators
     *
     * @param  MessageBus $message
     * @return null
     */
    protected function executeSanitizers($message)
    {
        foreach ($this->sanitizers as $sanitizer)
        {
            $instance = $this->app->make($sanitizer);

            if (!$instance instanceof Sanitizer)
            {
                $error = 'The class to decorate must be an implementation of Ahajji\Messager\Sanitizer';

                throw new InvalidArgumentException($error);
            }

            $instance->sanitize($message);
        }
    }

}